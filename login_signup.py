import tkinter as tk
from tkinter import messagebox
import sqlite3
import hashlib
import subprocess
import re

# Connect to SQLite database
conn = sqlite3.connect("user_data.db")
c = conn.cursor()

# Create a users table if it doesn't already exist
c.execute(
    """
    CREATE TABLE IF NOT EXISTS users (
        userID INTEGER PRIMARY KEY,
        username TEXT,
        password TEXT
    )
"""
)
conn.commit()
c.execute(
    """
CREATE TABLE IF NOT EXISTS simulations(
    simulationID INTEGER PRIMARY KEY, 
    name TEXT
)
"""
)
conn.commit()
c.execute(
    """
CREATE TABLE IF NOT EXISTS Created(
    userID INTEGER FORIEGN KEY,
    simulationID INTEGER FORIEGN KEY
)
"""
)


# Function to hash the password using SHA-256
def hash_password(password):
    return hashlib.sha256(password.encode()).hexdigest()


def validate_password(password):
    if re.match(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?!.*(.)\1{2})[a-zA-Z\d]{5,}$", password
    ):
        return True
    return False


def validate_username(username):
    # must be between 3 and 20 characters long
    if len(username) < 3 or len(username) > 20:
        return False

    # only contain letters, digits, underscores and dots
    if not re.match("^[a-zA-Z0-9_.]+$", username):
        return False

    return True


def register(event=None):
    # get username and password
    username = reg_username_entry.get()
    pass_entry = reg_password_entry.get()
    password = hash_password(pass_entry)

    if validate_password(pass_entry) == True and validate_username(username) == True:
        # check if the username already exists
        c.execute("SELECT * FROM users WHERE username=?", (username,))
        existing_user = c.fetchone()

        if existing_user:
            messagebox.showerror("Registration", "Username already taken")
        else:
            # insert into the database
            c.execute(
                "INSERT INTO users (username, password) VALUES (?, ?)",
                (username, password),
            )
            conn.commit()

            # clear registration
            reg_username_entry.delete(0, tk.END)
            reg_password_entry.delete(0, tk.END)

            # show registration success
            messagebox.showinfo("Registration", "Registration successful")
    else:
        messagebox.showerror("Invalid", "Username and/or password are invalid")


def login(event=None):
    # Get username and password from login entries
    username = username_entry.get()
    password = hash_password(password_entry.get())

    # Check if the entered username and hashed password match any user in the database
    c.execute(
        "SELECT * FROM users WHERE username=? AND password=?", (username, password)
    )
    user = c.fetchone()

    userID = user[0]

    if user:
        root.destroy()

        username = user[1]

        # Pass userID and username to menu_new.py using subprocess
        subprocess.run(
            ["python", "main.py", str(userID), username],
            capture_output=True,
            text=True,
        )
    else:
        messagebox.showerror("Login", "Login failed: invalid username or password")


# Create the main tkinter window
root = tk.Tk()
root.title("Login and Registration")
window_width = 1280
window_height = 720
root.geometry(f"{window_width}x{window_height}")
font = ("Helvetica", 20)

# instructions
instruction_label = tk.Label(root, text="Enter your credentials:", font=font)
instruction_label.pack()

# username instructions
username_instruct_label = tk.Label(
    root,
    text="only contain letters, digits, underscores and dots \n and a length of between 4 and 20",
)
username_instruct_label.place(x=150, y=75)

# registration instruction
reg_instruct_label = tk.Label(
    root,
    text="must contain at least one uppercase letter,\n one lowercase letter and one digit \n and a length greater 5",
)

reg_instruct_label.place(x=160, y=150)

# registration Section
reg_username_label = tk.Label(root, text="Username:", font=font)
reg_username_label.pack()
reg_username_entry = tk.Entry(root, font=font)
reg_username_entry.pack()

reg_password_label = tk.Label(root, text="Password:", font=font)
reg_password_label.pack()
reg_password_entry = tk.Entry(root, show="*", font=font)
reg_password_entry.pack()

register_button = tk.Button(root, text="Register", command=register, font=font)
register_button.pack()

# enter key: registration function
reg_username_entry.bind("<Return>", register)

# Login Section
username_label = tk.Label(root, text="Username:", font=font)
username_label.pack()
username_entry = tk.Entry(root, font=font)
username_entry.pack()

password_label = tk.Label(root, text="Password:", font=font)
password_label.pack()
password_entry = tk.Entry(root, show="*", font=font)
password_entry.pack()

login_button = tk.Button(root, text="Login", command=login, font=font)
login_button.pack()

# 'enter' key: login function
password_entry.bind("<Return>", login)


root.mainloop()
conn.close()
