import pygame
import math

# init pygame
pygame.init()

# constats
WIDTH, HEIGHT = 1280, 720
win = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Projectile Simulator")

# default proj
initial_speed = 50  # m/s
initial_angle = 45  # degrees
gravity = 9.8  # m/s^2
displacement = 0

# init projec
x, y = 100, HEIGHT - 100  # position vectors
vx = initial_speed * math.cos(math.radians(initial_angle))  # velocity vector x
vy = initial_speed * math.sin(math.radians(initial_angle))  # velocity vector y

# camera offset
offset_x = 0
offset_y = 0

# time step
dt = 0.1

# mainloop
clock = pygame.time.Clock()
running = True
launched = False

while running:
    # calc time of flight
    time_of_flight = (2 * vy) / gravity

    # calc time at the apex
    time_apex = time_of_flight / 2

    # calc vertical position at the apex
    y_apex = y + vy * time_apex - 0.5 * gravity * time_apex**2

    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    keys = pygame.key.get_pressed()

    # launch projectile
    if keys[pygame.K_SPACE]:
        launched = True

    if launched:
        # update projectile position
        x += vx * dt
        y -= vy * dt  # yaxis is invert
        vy -= gravity * dt

        # Update camera offset if proj leaves the screen
        if x - offset_x > WIDTH:
            offset_x += WIDTH
        elif x - offset_x < 0:
            offset_x -= WIDTH

        if y - offset_y > HEIGHT:
            offset_y += HEIGHT
        elif y - offset_y < 0:
            offset_y -= HEIGHT

        # reset proj if hits ground
        if y >= HEIGHT - 100:
            displacement = abs(x - 100)
            launched = False
            x, y = 100, HEIGHT - 100
            vx = initial_speed * math.cos(math.radians(initial_angle))
            vy = initial_speed * math.sin(math.radians(initial_angle))
            offset_x = 0
            offset_y = 0
            win.fill((255, 200, 200))

    else:
        # Menu and Update
        font = pygame.font.SysFont(None, 36)

        speed_text = font.render(
            f"Initial Speed: {round(initial_speed)} m/s (Hold E to increase the speed; Q to decrease)",
            True,
            (3, 1, 1),
        )
        angle_text = font.render(
            f"Initial Angle: {round(initial_angle)} degrees (Hold the UP arrow to increase the angle; DOWN arrow to decrease)",
            True,
            (1, 1, 3),
        )
        gravity_text = font.render(
            f"Gravity: {round(gravity, 2)} m/s^2 (Hold D to increase the gravity; A to decrease)",
            True,
            (1, 3, 1),
        )

        apex_prev_text = font.render(
            f"Apex: {round(y_apex - (HEIGHT - 100))}", True, (10, 10, 0)
        )

        displacement_text = font.render(
            f"Displacement: {round(displacement)}",
            True,
            (100, 100, 100),
        )

        instruction_text = font.render(("Press Space To Start"), True, (1, 3, 1))

        if keys[pygame.K_q]:
            initial_speed -= 1
        if keys[pygame.K_e]:
            initial_speed += 1
        if keys[pygame.K_UP]:
            initial_angle += 1
            if initial_angle > 90:
                initial_angle = 90
        if keys[pygame.K_DOWN]:
            initial_angle -= 1
            if initial_angle < 0:
                initial_angle = 0
        if keys[pygame.K_a]:
            gravity -= 0.1
        if keys[pygame.K_d]:
            gravity += 0.1

        vx = initial_speed * math.cos(math.radians(initial_angle))
        vy = initial_speed * math.sin(math.radians(initial_angle))

        win.blit(speed_text, (10, 10))
        win.blit(angle_text, (10, 50))
        win.blit(gravity_text, (10, 90))
        win.blit(apex_prev_text, (10, 130))
        win.blit(displacement_text, (10, 170))
        win.blit(instruction_text, (10, 210))

    # draw Projectile

    pygame.draw.circle(win, (230, 20, 0), (int(x - offset_x), int(y - offset_y)), 5)

    pygame.display.flip()
    win.fill((255, 255, 255))

pygame.quit()
