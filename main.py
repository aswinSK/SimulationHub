import pygame
import subprocess
from tkinter import filedialog, Tk, messagebox
import os
import sys
import sqlite3

# Extract userID and username from command-line arguments
if len(sys.argv) >= 3:
    userID = int(sys.argv[1])
    username = sys.argv[2]
else:
    userID = None
    username = None


pygame.init()

# screen
WIDTH, HEIGHT = 1280, 720
PADDING = 5

# colours
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (200, 200, 200)

# init screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Main Menu")


# Font
font = pygame.font.Font(None, 24)

# Labels + scripts
button_labels = [
    "Planets Simulator",
    "Projectile Simulator",
    "A* Pathfinding",
    "DFS",
    "BFS",
    "Add Simulation",
]
button_scripts = [
    "planet_sim.py",
    "projectile.py",
    "astar.py",
    "DFS.py",
    "BFS.py",
    None,
]

# Button spec
NUM_BUTTONS = len(button_labels)
BUTTON_HEIGHT = (HEIGHT - PADDING) // 3
BUTTON_WIDTH = WIDTH // NUM_BUTTONS

# List to store new file paths
dynamic_buttons = []

# start of retrieve of the created sims
conn = sqlite3.connect("user_data.db")
c = conn.cursor()
c.execute("SELECT simulationID FROM Created WHERE userID = (?)", (userID,))
createdsims = c.fetchall()

createdIDs = []

for i in createdsims:
    createdIDs.append(i[0])


conn.commit()
conn.close()
# end of retrieve


def add_sim():
    root = Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename()
    root.destroy()
    if file_path and len(dynamic_buttons) <= 6:
        dynamic_buttons.append(file_path)

        # connect to db
        conn = sqlite3.connect("user_data.db")
        c = conn.cursor()

        file_name = os.path.basename(file_path)

        c.execute("INSERT INTO simulations (name) VALUES (?)", (file_name,))

        c.execute("SELECT simulationID FROM simulations WHERE name=(?)", (file_name,))
        simulationID = c.fetchone()

        c.execute(
            "INSERT INTO Created (userID, simulationID) VALUES (?, ?)",
            (userID, int(simulationID[0])),
        )

        conn.commit()
        conn.close()


def add_sims_onload(createdIDs: list):
    conn = sqlite3.connect("user_data.db")
    c = conn.cursor()
    for i in createdIDs:
        # get the name of the simulation from the simulation table
        c.execute("SELECT name FROM simulations WHERE simulationID=(?)", (i,))
        name = c.fetchone()

        # load all of those files as buttons
        dynamic_buttons.append(name[0])
        # append to dynamic_buttons array

    conn.commit()
    conn.close()


def delete_additional_simulation():
    # Remove simulation from dynamic_buttons list
    del dynamic_buttons[i]

    # Connect to the database
    conn = sqlite3.connect("user_data.db")
    c = conn.cursor()

    # Get the simulation ID
    c.execute(
        "SELECT simulationID FROM simulations WHERE name = ?",
        (os.path.basename(file_path),),
    )
    simulationID = c.fetchone()

    # Delete the reference from the Created table
    c.execute(
        "DELETE FROM Created WHERE userID = ? AND simulationID = ?",
        (userID, simulationID[0]),
    )

    conn.commit()
    conn.close()


add_sims_onload(createdIDs)


# Main loop
running = True
while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Draw bottom buttons
    for i in range(NUM_BUTTONS):
        x = i * BUTTON_WIDTH
        y = HEIGHT - BUTTON_HEIGHT
        pygame.draw.rect(
            screen,
            GRAY,
            (
                x + PADDING,
                y + PADDING,
                BUTTON_WIDTH - 2 * PADDING,
                BUTTON_HEIGHT - 2 * PADDING,
            ),
        )

        text = font.render(button_labels[i], True, BLACK)
        rect = text.get_rect(center=(x + BUTTON_WIDTH // 2, y + BUTTON_HEIGHT // 2))
        screen.blit(text, rect)

    # Draw top buttons
    for i, file_path in enumerate(dynamic_buttons):
        x = i * BUTTON_WIDTH
        y = PADDING
        pygame.draw.rect(
            screen,
            GRAY,
            (
                x + PADDING,
                y + PADDING,
                BUTTON_WIDTH - 2 * PADDING,
                BUTTON_HEIGHT - 2 * PADDING,
            ),
        )

        # filename without extension as button label
        filename = os.path.basename(file_path)
        text = font.render(filename, True, BLACK)
        rect = text.get_rect(center=(x + BUTTON_WIDTH // 2, y + BUTTON_HEIGHT // 2))
        screen.blit(text, rect)

    # Display welcome message
    welcome_text = font.render(
        f"Hello {username}! Left Click to open, Right Click to delete, Left Click add simulation to open a file explorer, max 6 simulations",
        True,
        BLACK,
    )
    welcome_rect = welcome_text.get_rect(center=(WIDTH // 2, (HEIGHT // 4) + 100))
    screen.blit(welcome_text, welcome_rect)

    # check for clicks
    mouse_pos = pygame.mouse.get_pos()
    mouse_click = pygame.mouse.get_pressed()

    if mouse_click[0]:
        x, y = mouse_pos
        # check if top button was clicked
        for i, file_path in enumerate(dynamic_buttons):
            if (
                y < BUTTON_HEIGHT
                and x >= i * BUTTON_WIDTH
                and x < (i + 1) * BUTTON_WIDTH
            ):
                try:
                    subprocess.run(["python", file_path], check=True)
                except subprocess.CalledProcessError as e:
                    # Display error message to the user
                    messagebox.showerror(
                        "Error", "Failed to execute the selected script."
                    )

        # Check button was clicked
        if y >= HEIGHT - BUTTON_HEIGHT:
            # Slice up the bottom 3rd and find out which button was clicked
            button_index = x // BUTTON_WIDTH
            if button_scripts[button_index] is not None:
                try:
                    subprocess.run(["python", button_scripts[button_index]], check=True)
                except subprocess.CalledProcessError as e:
                    messagebox.showerror(
                        "Error", "Failed to execute the selected script."
                    )

            else:
                add_sim()

    if mouse_click[2]:
        x, y = mouse_pos
        for i, file_path in enumerate(dynamic_buttons):
            if (
                y < BUTTON_HEIGHT
                and x >= i * BUTTON_WIDTH
                and x < (i + 1) * BUTTON_WIDTH
            ):
                delete_additional_simulation()
                break
    pygame.display.update()
    screen.fill(WHITE)

pygame.quit()
