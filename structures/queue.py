from structures.struct import Struct


class Queue(Struct):
    def __init__(self):
        super().__init__()
        self.items = []

    def pull(self):
        if not self.is_empty():
            return self.items.pop(0)
