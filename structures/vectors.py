class Vector2:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __add__(self, other):
        if isinstance(other, Vector2):  # if the other object is a vector 2
            return Vector2(self.x + other.x, self.y + other.y)
        else:
            raise TypeError(
                f"unsupported operand types for addition: 'Vector2' and '{type(other).__name__}'"
            )

    def __sub__(self, other):
        if isinstance(other, Vector2):
            return Vector2(self.x - other.x, self.y - other.y)
        else:
            raise TypeError(
                f"Unsupported operand type(s) for -: 'Vector2' and '{type(other).__name__}'"
            )

    def __mul__(self, scalar):
        if isinstance(scalar, (int, float)):
            return Vector2(self.x * scalar, self.y * scalar)
        else:
            raise TypeError(
                f"Unsupported operand type(s) for *: 'Vector2' and '{type(scalar).__name__}'"
            )

    def __truediv__(self, scalar):
        if isinstance(scalar, (int, float)) and scalar != 0:
            return Vector2(self.x / scalar, self.y / scalar)
        else:
            raise ValueError(
                f"Division by zero or unsupported operand type for /: 'Vector2' and '{type(scalar).__name__}'"
            )

    def magnitude(self):
        return (self.x**2 + self.y**2) ** 0.5

    def magnitude_squared(self):
        return self.x**2 + self.y**2

    def normalise(self):  # returns a vector of 1 mag
        mag = self.magnitude()
        if mag != 0:
            return self / mag
        else:
            raise ValueError("Cannot normalize a zero vector")

    def rotate(self, angle):
        import math

        radians = math.radians(angle)
        cos_theta = math.cos(radians)
        sin_theta = math.sin(radians)
        return Vector2(
            self.x * cos_theta - self.y * sin_theta,
            self.x * sin_theta + self.y * cos_theta,
        )

    def elementwise(self):
        return self.x, self.y
