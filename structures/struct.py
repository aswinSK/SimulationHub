class Struct:
    def __init__(self):
        self.items = []

    def is_empty(self):
        if len(self.items) == 0:
            return True

    def peek(self):
        if not self.is_empty():
            return self.items[0]

    def size(self):
        return len(self.items)

    def push(self, item):
        self.items.append(item)

    def get_items(self):
        return self.items

    def empty(self):
        self.items = []
