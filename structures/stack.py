from structures.struct import Struct


class Stack(Struct):
    def __init__(self):
        super().__init__()
        self.items = []

    def pop(self):
        if not self.is_empty():
            return self.items.pop()
