import pygame
from structures.queue import Queue
from node import Node

FULL_WIDTH = 1280
WIDTH = 750
WIN = pygame.display.set_mode((FULL_WIDTH, WIDTH))
pygame.display.set_caption("BFS")


WHITE = (255, 255, 255)
GREY = (128, 128, 128)
RED = (240, 40, 40)
GREEN = (23, 87, 39)
BLACK = (0, 0, 0)


# Functions for creating and drawing the grid
def make_grid(rows, width):
    grid = []
    gap = width // rows
    for i in range(rows):
        grid.append([])
        for j in range(rows):
            spot = Node(i, j, gap, rows)
            grid[i].append(spot)

    return grid


def draw_instructions(win):
    font = pygame.font.SysFont("Arial", 30)
    instructions = [
        "Left Click First: Place Start (Orange)",
        "Left Click Second: Place End (Blue)",
        "All subsequent left clicks are barriers",
        "",
        "Right Click will delete the node",
        "If the node was a start or end the next",
        "node places will be that node",
        "",
        "Spacebar: Run Algorithm",
        "C Key: Clear Grid",
        "",
        "Red: Searched",
        "Green: to be searched next",
        "Purple: path",
    ]
    y_offset = 10
    for instruction in instructions:
        text = font.render(instruction, True, BLACK)
        win.blit(text, (760, y_offset))
        y_offset += 35


def draw_grid(win, rows, width):
    gap = width // rows
    for i in range(rows):
        pygame.draw.line(win, GREY, (0, i * gap), (width, i * gap))
        for j in range(rows):
            pygame.draw.line(win, GREY, (j * gap, 0), (j * gap, width))


def draw(win, grid, rows, width):
    win.fill(WHITE)

    for row in grid:
        for spot in row:
            spot.draw(win)

    draw_grid(win, rows, width)
    draw_instructions(win)
    pygame.display.update()


def get_clicked_pos(pos, rows, width):
    space = width // rows
    y, x = pos
    row = y // space
    col = x // space
    return row, col


def bfs_algorithm(draw, start, end):
    queue = Queue()
    queue.push(start)
    came_from = {start: None}
    path_found = False

    while not queue.is_empty() and not path_found:
        current = queue.pull()

        if current == end:
            reconstruct_path_bfs(came_from, end, draw)
            end.make_end()
            path_found = True
            break

        for neighbour in current.neighbours:
            if neighbour not in came_from:
                queue.push(neighbour)
                came_from[neighbour] = current
                neighbour.make_open()

        if current != start:
            current.make_closed()

        draw()

    return path_found


def reconstruct_path_bfs(came_from, end, draw):
    current = end
    while current is not None and current in came_from:
        current = came_from[current]
        if current is not None:
            current.make_path()
            draw()


# Main function to run the program
def main(win, width):
    rows = 50
    grid = make_grid(rows, width)

    start = None
    end = None

    run = True
    pygame.font.init()
    while run:
        draw(win, grid, rows, width)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if pygame.mouse.get_pressed()[0]:  # Left mouse button
                pos = pygame.mouse.get_pos()
                row, col = get_clicked_pos(pos, rows, width)
                try:
                    spot = grid[row][col]

                    if not start and spot != end:
                        start = spot
                        start.make_start()
                    elif not end and spot != start:
                        end = spot
                        end.make_end()
                    elif spot != end and spot != start:
                        spot.make_barrier()
                except:
                    pass

            elif pygame.mouse.get_pressed()[2]:  # Right mouse button
                pos = pygame.mouse.get_pos()
                row, col = get_clicked_pos(pos, rows, width)
                try:
                    spot = grid[row][col]
                    spot.reset()
                    if spot == start:
                        start = None
                    elif spot == end:
                        end = None
                except:
                    pass

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and start and end:
                    for row in grid:
                        for spot in row:
                            spot.update_neighbours(grid)
                    bfs_algorithm(lambda: draw(win, grid, rows, width), start, end)

                if event.key == pygame.K_c:
                    start = None
                    end = None
                    grid = make_grid(rows, width)

                if event.key == pygame.K_ESCAPE:
                    run = False

    pygame.quit()


if __name__ == "__main__":
    main(WIN, WIDTH)
