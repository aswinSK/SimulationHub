# The original code stored a list of points for each planet for drawing the
# trails, which were re-drawn to the screen every frame. These list grew equal
# to the number of frames passed, which meant more and more lines needed to be
# drawn the longer the program ran. This gives a time and memory complexity of
# O(p * f), where p is the number of planets, and f is the number of frames
# elapsed.

# This implementation instead uses a seperate Surface onto which the trails are
# drawn once, then each frame, this Surface is blit to the screen. The screen
# can be wiped, overwritten, or whatever, but the trails remain in the second
# Surface.

# This implementation also uses the vector form of Newton's law of gravitation,
# using pygame.math Vector2. This avoids multiple expensive trig functions in
# exchange for a single sqrt, and a cleaner code layout. Additionally, since
# the force is equal and opposite for both bodies in an interaction, we only
# need to calculate the force for any unique pairing of planets once, and just
# apply the same force to both.

# References:
# https://en.wikipedia.org/wiki/Newtons_law_of_universal_gravitation#Vector_form

import pygame
from pygame import Color
from structures.vectors import Vector2
from itertools import combinations


speed_mod = 1
# Constants:
# px
WIDTH = 1280
HEIGHT = 720

DELTATIME = 24 * 60 * 60 * speed_mod

GRAVITATION = 6.67408e-11

# Kilograms
SUN_MASS = 1.98892 * 10**30
EARTH_MASS = 5.9742 * 10**23
MARS_MASS = 6.39 * 10**23
MERCURY_MASS = 3.3 * 10**23
VENUS_MASS = 4.8685 * 10**24
JUPITER_MASS = 1.898 * 10**26
SATURN_MASS = 5.683 * 10**26
URANUS_MASS = 8.681 * 10**25
NEPTUNE_MASS = 1.024 * 10**26

scalar = 50
AU = 149.6e9  # meters
SCALE = scalar / AU  # px / AU

pygame.init()

FONT = pygame.font.SysFont("Mono", 18)
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
TRAILS = pygame.Surface((WIDTH, HEIGHT))


class Planet:
    def __init__(
        self,
        position: Vector2,
        velocity: Vector2,
        mass: float,
        radius: float,
        color: Color,
    ):
        self.position = position
        self.velocity = velocity
        self.radius = radius
        self.mass = mass
        self.color = color
        self.old_position = Vector2(0, 0)
        self.force = Vector2(0, 0)

    def draw(self):
        position = (self.position * SCALE) + Vector2(WIDTH / 2, HEIGHT / 2)
        old_position = (self.old_position * SCALE) + Vector2(WIDTH / 2, HEIGHT / 2)
        pygame.draw.line(
            TRAILS,
            self.color,
            (int(old_position.x), int(old_position.y)),
            (int(position.x), int(position.y)),
        )
        pygame.draw.circle(
            WINDOW, self.color, (int(position.x), int(position.y)), self.radius
        )

    def draw_no_circle(self):
        position = (self.position * SCALE) + Vector2(WIDTH / 2, HEIGHT / 2)
        old_position = (self.old_position * SCALE) + Vector2(WIDTH / 2, HEIGHT / 2)
        pygame.draw.line(
            TRAILS,
            self.color,
            (int(old_position.x), int(old_position.y)),
            (int(position.x), int(position.y)),
        )


class System:
    def __init__(self, planets: list[Planet]):
        self.planets = planets

    def draw(self):
        for planets in self.planets:
            planets.draw()

    def draw_no_circle(self):
        for planets in self.planets:
            planets.draw_no_circle()

    def evolve(self):
        # combinations returns only unique pairings of elements
        for obj1, obj2 in combinations(self.planets, r=2):
            unit = (obj2.position - obj1.position).normalise()
            distance_squared = (obj2.position - obj1.position).magnitude_squared()
            force_magnitude = GRAVITATION * (obj1.mass * obj2.mass) / distance_squared
            force = unit * force_magnitude

            # Apply forces to both planets
            obj1.force += force
            obj2.force -= force

        for obj in self.planets:
            obj.old_position = obj.position
            obj.velocity += obj.force / obj.mass * DELTATIME
            obj.position += obj.velocity * DELTATIME
            obj.force = Vector2(0, 0)  # Reset the force


def display_instructions(scale):
    instructions = [
        "UP arrow key to increase scale.",
        "DOWN arrow key to decrease scale.",
        "RIGHT arror key to increase speed.",
        "LEFT arror key to decrease speed.",
        f"Scale: {scale} pixels per astronomical unit",
        f"Speed: {round(speed_mod, 2)}",
    ]
    y_offset = 20
    for instruction in instructions:
        text_surface = FONT.render(instruction, True, Color("WHITE"))
        WINDOW.blit(text_surface, (10, y_offset))
        y_offset += 20


def main():
    global scalar, SCALE, speed_mod, DELTATIME

    system = System(
        [
            Planet(
                Vector2(0, 0),
                Vector2(0, 0),
                SUN_MASS,
                15,
                Color("YELLOW"),
            ),
            Planet(
                Vector2(-1 * AU, 0), Vector2(0, 29783), EARTH_MASS, 8, Color("BLUE")
            ),
            Planet(
                Vector2(-1.524 * AU, 0), Vector2(0, 25077), MARS_MASS, 6, Color("RED")
            ),
            Planet(
                Vector2(-0.387 * AU, 0),
                Vector2(0, 47500),
                MERCURY_MASS,
                4,
                Color("GRAY"),
            ),
            Planet(
                Vector2(-0.723 * AU, 0),
                Vector2(0, 35020),
                VENUS_MASS,
                7,
                Color("WHITE"),
            ),
            Planet(
                Vector2(-2.2 * AU, 0),
                Vector2(0, 20000),
                JUPITER_MASS,
                11,
                Color("ORANGE"),
            ),
            Planet(
                Vector2(9.72 * AU, 0), Vector2(0, 9470), SATURN_MASS, 9, Color("ORANGE")
            ),
            Planet(
                Vector2(19.6 * AU, 0), Vector2(0, 6660), URANUS_MASS, 8, Color("BLUE")
            ),
            Planet(
                Vector2(29.9 * AU, 0), Vector2(0, 5460), NEPTUNE_MASS, 10, Color("BLUE")
            ),
        ]
    )

    clock = pygame.time.Clock()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    scalar += 5
                    SCALE = scalar / AU
                    TRAILS.fill((0, 0, 0))
                elif event.key == pygame.K_DOWN:
                    scalar -= 5
                    if scalar < 10:
                        scalar = 10
                    SCALE = scalar / AU
                    TRAILS.fill((0, 0, 0))
                elif event.key == pygame.K_RIGHT:
                    speed_mod += 0.1
                    if speed_mod > 2:
                        speed_mod = 2
                    DELTATIME = 24 * 60 * 60 * speed_mod
                    TRAILS.fill((0, 0, 0))
                elif event.key == pygame.K_LEFT:
                    speed_mod -= 0.1
                    if speed_mod < 0.5:
                        speed_mod = 0.5
                    DELTATIME = 24 * 60 * 60 * speed_mod
                    TRAILS.fill((0, 0, 0))

        clock.get_fps()
        fps = str(int(clock.get_fps()))

        WINDOW.fill((0, 0, 0))
        WINDOW.blit(TRAILS, (0, 0))
        WINDOW.blit(FONT.render(fps, True, Color("RED")), (0, 0))
        display_instructions(scalar)

        system.evolve()
        if scalar >= 40:
            system.draw()
        else:
            system.draw_no_circle()

        pygame.display.flip()  # flip is preferred, as we then don't need to clear the screen.

        clock.tick()


if __name__ == "__main__":
    main()
