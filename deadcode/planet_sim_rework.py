#!/usr/bin/python3

# The original code stored a list of points for each planet for drawing the
# trails, which were re-drawn to the screen every frame. These list grew equal
# to the number of frames passed, which meant more and more lines needed to be
# drawn the longer the program ran. This gives a time and memory complexity of
# O(p * f), where p is the number of planets, and f is the number of frames
# elapsed.

# This implementation instead uses a seperate Surface onto which the trails are
# drawn once, then each frame, this Surface is blit to the screen. The screen
# can be wiped, overwritten, or whatever, but the trails remain in the second
# Surface.

# This implementation also uses the vector form of Newton's law of gravitation,
# using pygame.math Vector2. This avoids multiple expensive trig functions in
# exchange for a single sqrt, and a cleaner code layout. Additionally, since
# the force is equal and opposite for both bodies in an interaction, we only
# need to calculate the force for any unique pairing of planets once, and just
# apply the same force to both.

# References:
# https://en.wikipedia.org/wiki/Newton's_law_of_universal_gravitation#Vector_form

import pygame
from pygame import Color
from pygame.math import Vector2

from itertools import combinations

# Constants:

# px
WIDTH = 1280
HEIGHT = 720

DELTATIME = 24 * 60 * 60

GRAVITATION = 6.67408e-11

# Kilograms
SUN_MASS = 1.98892 * 10**30
EARTH_MASS = 5.9742 * 10**23
MARS_MASS = 6.39 * 10**23
MERCURY_MASS = 3.3 * 10**23
VENUS_MASS = 4.8685 * 10**24
JUPITER_MASS = 1.898 * 10**26
PLUTO_MASS = 1.303 * 10**22

AU = 149.6e9  # meters
SCALE = 80 / AU  # px / AU

###

pygame.init()

FONT = pygame.font.SysFont("Mono", 18)
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
TRAILS = pygame.Surface((WIDTH, HEIGHT))


class Object:
    def __init__(
        self,
        position: Vector2,
        velocity: Vector2,
        mass: float,
        radius: float,
        color: Color,
    ):
        self.position = position
        self.velocity = velocity
        self.radius = radius
        self.mass = mass
        self.color = color
        self.old_position = Vector2(0, 0)
        self.force = Vector2(0, 0)

    def draw(self):
        position = (self.position * SCALE) + Vector2(WIDTH / 2, HEIGHT / 2)
        old_position = (self.old_position * SCALE) + Vector2(WIDTH / 2, HEIGHT / 2)
        pygame.draw.line(TRAILS, self.color, old_position, position)
        pygame.draw.circle(WINDOW, self.color, position, self.radius)


class System:
    def __init__(self, objects: list[Object]):
        self.objects = objects

    def draw(self):
        for object in self.objects:
            object.draw()

    def evolve(self):
        # combinations returns only unique pairings of elements
        for obj1, obj2 in combinations(self.objects, r=2):
            unit = (obj1.position - obj2.position) / (
                obj1.position - obj2.position
            ).magnitude()
            force = (
                -GRAVITATION
                * (obj1.mass * obj2.mass)
                / (obj1.position - obj2.position).magnitude_squared()
                * unit
            )

            # both objects experience the same force, just in opposing directions
            obj1.force += force
            obj2.force -= force

        for object in self.objects:
            object.old_position = object.position
            object.velocity += object.force / object.mass * DELTATIME
            object.position += object.velocity * DELTATIME
            object.force = Vector2(0, 0)  # reset the force to prevent funny business


def main():
    system = System(
        [
            Object(
                Vector2(0, 0),
                Vector2(0, 0),
                SUN_MASS,
                15,
                Color("YELLOW"),
            ),
            Object(
                Vector2(-1 * AU, 0), Vector2(0, 29783), EARTH_MASS, 8, Color("BLUE")
            ),
            Object(
                Vector2(-1.524 * AU, 0), Vector2(0, 25077), MARS_MASS, 6, Color("RED")
            ),
            Object(
                Vector2(-0.387 * AU, 0),
                Vector2(0, 47500),
                MERCURY_MASS,
                4,
                Color("GRAY"),
            ),
            Object(
                Vector2(-0.723 * AU, 0),
                Vector2(0, 35020),
                VENUS_MASS,
                7,
                Color("WHITE"),
            ),
            Object(
                Vector2(-2.2 * AU, 0),
                Vector2(0, 20000),
                JUPITER_MASS,
                8,
                Color("ORANGE"),
            ),
            Object(
                Vector2(-4 * AU, 0),
                Vector2(5000, 15005),
                PLUTO_MASS,
                4,
                Color("ORANGE"),
            ),
        ]
    )

    clock = pygame.time.Clock()

    while True:
        if pygame.QUIT in map(lambda e: e.type, pygame.event.get()):
            break

        clock.get_fps()
        fps = str(int(clock.get_fps()))

        WINDOW.blit(TRAILS, (0, 0))
        WINDOW.blit(FONT.render(fps, True, Color("RED")), (0, 0))

        system.evolve()
        system.draw()

        pygame.display.flip()  # flip is preferred, as we then don't need to clear the screen.

        clock.tick()


if __name__ == "__main__":
    main()
