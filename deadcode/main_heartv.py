"""
It imports the necessary libraries, including pygame and subprocess.
Initializes Pygame and sets up some constants for the screen dimensions, colors, and button properties.
Defines the main menu screen and sets the caption.
Creates a font object for rendering text on buttons.
Defines labels and corresponding scripts for each button.
Defines a class LikeButton for creating buttons on the screen.
Instantiates heart buttons based on the defined labels.
Sets up the main loop for the application.
Handles events such as quitting the application and mouse clicks.
Checks for clicks on the heart buttons and executes corresponding actions.
Draws the buttons on the screen.
Updates the display and fills the screen with a white background.
Quits Pygame when the main loop exits.
"""

import pygame
import subprocess

pygame.init()

# Screen
WIDTH, HEIGHT = 1280, 720

PADDING = 5

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (200, 200, 200)
RED = (255, 0, 0)

# init screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Main Menu")

# Font
font = pygame.font.Font(None, 16)

# Labels + scripts
button_labels = [
    "Planets Simulator",
    "Projectile Simulator",
    "A* Pathfinding",
    "DFS",
    "BFS",
]
button_scripts = [
    "planet_sim_rework.py",
    "projectile.py",
    "astar.py",
    "DFS.py",
    "BFS.py",
]


# Like button
class LikeButton:
    def __init__(self, x, y, width, height, label, color=GRAY):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = color
        self.clicked = False
        self.label = label

    def draw(self):
        pygame.draw.rect(screen, self.color, (self.x, self.y, self.width, self.height))

    def check_click(self, mouse_pos):
        if (
            self.x <= mouse_pos[0] <= self.x + self.width
            and self.y <= mouse_pos[1] <= self.y + self.height
        ):
            if not self.clicked:
                self.clicked = True
                self.color = RED
                self.turn_red()
            else:
                self.clicked = False
                self.color = GRAY
                self.turn_gray()

    def turn_red(self):
        # Function to run when the button is clicked
        print(
            f"{self.label} button clicked. State: {'Clicked' if self.clicked else 'Not Clicked'}"
        )

    def turn_gray(self):
        # Another function to run when the button is clicked
        print(
            f"{self.label} button clicked again. State: {'Clicked' if self.clicked else 'Not Clicked'}"
        )


NUM_BUTTONS = len(button_labels)
BUTTON_HEIGHT = HEIGHT // 3
BUTTON_WIDTH = WIDTH // NUM_BUTTONS


# Create like buttons with rectangles
like_buttons = [
    LikeButton(
        (i * BUTTON_WIDTH) + (BUTTON_WIDTH // 2) - 120,  # Adjusted for centering
        HEIGHT - (BUTTON_HEIGHT // 4) - 220,
        BUTTON_WIDTH - 15,  # Adjusted width
        BUTTON_HEIGHT // 8,  # Adjusted height
        label,
    )
    for i, label in enumerate(button_labels)
]


# Main loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            # Left mouse button clicked
            mouse_pos = pygame.mouse.get_pos()
            for button in like_buttons:
                button.check_click(mouse_pos)

            # Check if any of the simulation buttons were clicked
            for i in range(NUM_BUTTONS):
                button_rect = pygame.Rect(
                    i * BUTTON_WIDTH,
                    HEIGHT - BUTTON_HEIGHT,
                    BUTTON_WIDTH,
                    BUTTON_HEIGHT,
                )
                if button_rect.collidepoint(mouse_pos):
                    subprocess.run(["python", button_scripts[i]])

    # Draw the buttons using slicing
    for i in range(NUM_BUTTONS):
        x = i * BUTTON_WIDTH
        y = HEIGHT - BUTTON_HEIGHT
        pygame.draw.rect(
            screen,
            GRAY,
            (
                x + PADDING,
                y + PADDING,
                BUTTON_WIDTH - 2 * PADDING,
                BUTTON_HEIGHT - 2 * PADDING,
            ),
        )

        text = font.render(button_labels[i], True, BLACK)
        rect = text.get_rect(center=(x + BUTTON_WIDTH // 2, y + BUTTON_HEIGHT // 2))
        screen.blit(text, rect)

        # Draw heart buttons
        like_buttons[i].draw()

    pygame.display.update()
    screen.fill(WHITE)

pygame.quit()
