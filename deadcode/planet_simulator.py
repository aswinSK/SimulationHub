from deadcode.planet_dead import *

pygame.init()

WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Planet Simulation")

# masses
SUN_MASS = 1.98892 * 10**30
EARTH_MASS = 5.9742 * 10**23
MARS_MASS = 6.39 * 10**23
MERCURY_MASS = 3.3 * 10**23
VENUS_MASS = 4.8685 * 10**24
MOON_MASS = 7.34767309 * 10**24
JUPITER_MASS = 1.898 * 10**26
ASTERIOD_MASS = 2 * 10**22
ASTERIOD_MASS2 = 2 * 10**23

ROCKET_MASS = 2400

# colours
WHITE = (255, 225, 225)
YELLOW = (255, 255, 0)
BLUE = (100, 149, 237)
RED = (188, 39, 50)
DARK_GREY = (80, 78, 81)
ORANGE = (255, 165, 0)
pink = (250, 0, 128)
GREEN = (0, 255, 0)

FONT = pygame.font.SysFont("comicsans", 16)


def main():
    run = True
    clock = pygame.time.Clock()

    # init planets + stars
    sun = Planet(0, 0, 30, YELLOW, SUN_MASS)
    sun.sun = True

    earth = Planet(-1 * Planet.AU, 0, 16, BLUE, EARTH_MASS)
    earth.y_vel = 29.783 * 1000
    mars = Planet(-1.524 * Planet.AU, 0, 12, RED, MARS_MASS)
    mars.y_vel = 25.077 * 1000
    mercury = Planet(0.387 * Planet.AU, 0, 8, DARK_GREY, MERCURY_MASS)
    mercury.y_vel = -47.4 * 1000
    venus = Planet(0.723 * Planet.AU, 0, 14, WHITE, VENUS_MASS)
    venus.y_vel = -35.02 * 1000
    jupiter = Planet(2.2 * Planet.AU, 0, 15, ORANGE, JUPITER_MASS)
    jupiter.y_vel = -20 * 1000
    planets = [sun, earth, mars, mercury, venus, jupiter]

    while run:
        clock.tick(60)  # FPS
        WIN.fill((0, 0, 0))

        mouse_pos = pygame.mouse.get_pos()
        # mp_au = pixels_to_au(mouse_pos)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        for planet in planets:
            planet.update_position(planets)
            planet.draw(WIN)

        pygame.display.update()

    pygame.quit()


main()
