import pygame
import subprocess
import tkinter as tk
from tkinter import filedialog

pygame.init()

# Screen
WIDTH, HEIGHT = 1280, 720

# Colors
WHITE = (255, 255, 255)
GRAY = (200, 200, 200)
RED = (255, 0, 0)

# Font
font = pygame.font.Font(None, 16)

# Button properties
PADDING = 5
BUTTON_HEIGHT = 50
BUTTON_WIDTH = WIDTH - 2 * PADDING

# Button labels and scripts
button_labels = [
    "Planets Simulator",
    "Projectile Simulator",
    "A* Pathfinding",
    "DFS",
    "BFS",
]
button_scripts = {
    "Planets Simulator": "planet_sim_rework.py",
    "Projectile Simulator": "projectile.py",
    "A* Pathfinding": "astar.py",
    "DFS": "DFS.py",
    "BFS": "BFS.py",
}


# Function to add a new simulation button
def add_simulation():
    root = tk.Tk()
    root.withdraw()
    filepath = filedialog.askopenfilename()
    if filepath:
        script_label = filepath.split("/")[-1]
        button_labels.insert(-1, script_label)
        button_scripts[script_label] = filepath


# Initialize screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Main Menu")

# Main loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            mouse_pos = pygame.mouse.get_pos()
            for i, label in enumerate(button_labels):
                if (
                    HEIGHT - (i + 2) * BUTTON_HEIGHT
                    <= mouse_pos[1]
                    <= HEIGHT - (i + 1) * BUTTON_HEIGHT
                ):
                    if label == "Add Simulation":
                        add_simulation()
                    else:
                        subprocess.run(["python", button_scripts[label]])

    screen.fill(WHITE)

    # Draw buttons
    for i, label in enumerate(button_labels):
        button_y = HEIGHT - (i + 1) * BUTTON_HEIGHT
        button_color = RED if label == "Add Simulation" else GRAY
        pygame.draw.rect(
            screen,
            button_color,
            (PADDING, button_y, BUTTON_WIDTH, BUTTON_HEIGHT - PADDING),
        )

        # Button text
        text = font.render(label, True, (0, 0, 0))
        text_rect = text.get_rect(center=(WIDTH // 2, button_y + BUTTON_HEIGHT // 2))
        screen.blit(text, text_rect)

    pygame.display.update()

pygame.quit()
