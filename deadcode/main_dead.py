import pygame
import subprocess
from tkinter import messagebox
import logging

pygame.init()

# Screen
WIDTH, HEIGHT = 1280, 720
PADDING = 5

# Colours
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (200, 200, 200)

# init screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Main Menu")

# Font
font = pygame.font.Font(None, 16)

# Labels + scripts
button_labels = [
    "Planets Simulator",
    "Projectile Simulator",
    "A* Pathfinding",
    "DFS",
    "BFS",
]
button_scripts = [
    "planet_sim_rework_copy.py",
    "projectile.py",
    "astar.py",
    "DFS.py",
    "BFS.py",
    "add_simulation.py",
]

# Button spec
NUM_BUTTONS = len(button_labels)
BUTTON_HEIGHT = HEIGHT // 3
BUTTON_WIDTH = WIDTH // NUM_BUTTONS

# Configure logging
logging.basicConfig(filename="error.log", level=logging.ERROR)


# Main loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Draw buttons
    for i in range(NUM_BUTTONS):
        x = i * BUTTON_WIDTH
        y = HEIGHT - BUTTON_HEIGHT
        pygame.draw.rect(
            screen,
            GRAY,
            (
                x + PADDING,
                y + PADDING,
                BUTTON_WIDTH - 2 * PADDING,
                BUTTON_HEIGHT - 2 * PADDING,
            ),
        )

        text = font.render(button_labels[i], True, BLACK)
        rect = text.get_rect(center=(x + BUTTON_WIDTH // 2, y + BUTTON_HEIGHT // 2))
        screen.blit(text, rect)

    # Check for clicks
    mouse_pos = pygame.mouse.get_pos()
    mouse_click = pygame.mouse.get_pressed()

    if mouse_click[0]:
        x, y = mouse_pos
        if y >= HEIGHT - BUTTON_HEIGHT:
            # Slice up the bottom 3rd and find out which button was clicked
            button_index = x // BUTTON_WIDTH
            try:
                subprocess.run(["python", button_scripts[button_index]], check=True)
            except subprocess.CalledProcessError as e:
                # Display error message to the user
                messagebox.showerror("Error", "Failed to execute the selected script.")
                # Log the error
                logging.error("Error executing file:", exc_info=True)

    font = pygame.font.SysFont(None, 26)
    pygame.display.update()
    screen.fill(WHITE)

pygame.quit()
